﻿namespace Fitnessify {
	partial class AddConsumedFoodDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.AddButton = new System.Windows.Forms.Button();
			this.GiveUpButton = new System.Windows.Forms.Button();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.foodBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.foodBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.SuspendLayout();
			// 
			// AddButton
			// 
			this.AddButton.Location = new System.Drawing.Point(13, 65);
			this.AddButton.Name = "AddButton";
			this.AddButton.Size = new System.Drawing.Size(144, 23);
			this.AddButton.TabIndex = 0;
			this.AddButton.Text = "Dodaj";
			this.AddButton.UseVisualStyleBackColor = true;
			this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// GiveUpButton
			// 
			this.GiveUpButton.Location = new System.Drawing.Point(13, 94);
			this.GiveUpButton.Name = "GiveUpButton";
			this.GiveUpButton.Size = new System.Drawing.Size(144, 23);
			this.GiveUpButton.TabIndex = 1;
			this.GiveUpButton.Text = "Odustani";
			this.GiveUpButton.UseVisualStyleBackColor = true;
			this.GiveUpButton.Click += new System.EventHandler(this.GiveUpButton_Click);
			// 
			// comboBox1
			// 
			this.comboBox1.DataSource = this.foodBindingSource;
			this.comboBox1.DisplayMember = "ListInfo";
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(12, 12);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(145, 21);
			this.comboBox1.TabIndex = 2;
			// 
			// foodBindingSource
			// 
			this.foodBindingSource.DataSource = typeof(Fitnessify.Model.Food);
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(13, 40);
			this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(144, 20);
			this.numericUpDown1.TabIndex = 3;
			this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// AddConsumedFoodDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(169, 129);
			this.Controls.Add(this.numericUpDown1);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.GiveUpButton);
			this.Controls.Add(this.AddButton);
			this.Name = "AddConsumedFoodDialog";
			((System.ComponentModel.ISupportInitialize)(this.foodBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button AddButton;
		private System.Windows.Forms.Button GiveUpButton;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.BindingSource foodBindingSource;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
	}
}