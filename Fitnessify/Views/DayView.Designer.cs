﻿namespace Fitnessify {
    partial class DayView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.consumedFoodsListBox = new System.Windows.Forms.ListBox();
			this.consumedFoodBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.AddConsumedFoodButton = new System.Windows.Forms.Button();
			this.deleteConsumedFoodButton = new System.Windows.Forms.Button();
			this.deleteDoneActivityButton = new System.Windows.Forms.Button();
			this.addDoneActivityButton = new System.Windows.Forms.Button();
			this.doneActivitiesListBox = new System.Windows.Forms.ListBox();
			this.iDoneActivityBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.foodManagementButton = new System.Windows.Forms.Button();
			this.activitiesManagementButton = new System.Windows.Forms.Button();
			this.personalDataButton = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.goalWeightDeadlineLabel = new System.Windows.Forms.Label();
			this.goalWeightLabel = new System.Windows.Forms.Label();
			this.genderLabel = new System.Windows.Forms.Label();
			this.ageLabel = new System.Windows.Forms.Label();
			this.weightLabel = new System.Windows.Forms.Label();
			this.heightLabel = new System.Windows.Forms.Label();
			this.basalMetabolismLabel = new System.Windows.Forms.Label();
			this.bmiLabel = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.currentDayDeficitLabel = new System.Windows.Forms.Label();
			this.dailyCaloricDeficitLabel = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.carbsPercLabel = new System.Windows.Forms.Label();
			this.proteinsPercLabel = new System.Windows.Forms.Label();
			this.carbsLabel = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.proteinsLabel = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.caloriesLabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.fatsPercLabel = new System.Windows.Forms.Label();
			this.fatsLabel = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.consumedFoodBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.iDoneActivityBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(12, 12);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker1.TabIndex = 0;
			this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
			// 
			// consumedFoodsListBox
			// 
			this.consumedFoodsListBox.DataSource = this.consumedFoodBindingSource;
			this.consumedFoodsListBox.DisplayMember = "ListInfo";
			this.consumedFoodsListBox.FormattingEnabled = true;
			this.consumedFoodsListBox.Location = new System.Drawing.Point(12, 38);
			this.consumedFoodsListBox.Name = "consumedFoodsListBox";
			this.consumedFoodsListBox.Size = new System.Drawing.Size(200, 160);
			this.consumedFoodsListBox.TabIndex = 2;
			// 
			// consumedFoodBindingSource
			// 
			this.consumedFoodBindingSource.DataSource = typeof(Fitnessify.Model.ConsumedFood);
			// 
			// AddConsumedFoodButton
			// 
			this.AddConsumedFoodButton.Location = new System.Drawing.Point(218, 38);
			this.AddConsumedFoodButton.Name = "AddConsumedFoodButton";
			this.AddConsumedFoodButton.Size = new System.Drawing.Size(137, 23);
			this.AddConsumedFoodButton.TabIndex = 3;
			this.AddConsumedFoodButton.Text = "Dodaj konzumiranu hranu";
			this.AddConsumedFoodButton.UseVisualStyleBackColor = true;
			this.AddConsumedFoodButton.Click += new System.EventHandler(this.AddConsumedFoodButton_Click);
			// 
			// deleteConsumedFoodButton
			// 
			this.deleteConsumedFoodButton.Location = new System.Drawing.Point(218, 67);
			this.deleteConsumedFoodButton.Name = "deleteConsumedFoodButton";
			this.deleteConsumedFoodButton.Size = new System.Drawing.Size(137, 23);
			this.deleteConsumedFoodButton.TabIndex = 4;
			this.deleteConsumedFoodButton.Text = "Obriši konzumiranu hranu";
			this.deleteConsumedFoodButton.UseVisualStyleBackColor = true;
			this.deleteConsumedFoodButton.Click += new System.EventHandler(this.deleteConsumedFoodButton_Click);
			// 
			// deleteDoneActivityButton
			// 
			this.deleteDoneActivityButton.Location = new System.Drawing.Point(218, 233);
			this.deleteDoneActivityButton.Name = "deleteDoneActivityButton";
			this.deleteDoneActivityButton.Size = new System.Drawing.Size(137, 23);
			this.deleteDoneActivityButton.TabIndex = 7;
			this.deleteDoneActivityButton.Text = "Obriši izvršenu aktivnost";
			this.deleteDoneActivityButton.UseVisualStyleBackColor = true;
			this.deleteDoneActivityButton.Click += new System.EventHandler(this.deleteDoneActivityButton_Click);
			// 
			// addDoneActivityButton
			// 
			this.addDoneActivityButton.Location = new System.Drawing.Point(218, 204);
			this.addDoneActivityButton.Name = "addDoneActivityButton";
			this.addDoneActivityButton.Size = new System.Drawing.Size(137, 23);
			this.addDoneActivityButton.TabIndex = 6;
			this.addDoneActivityButton.Text = "Dodaj izvršenu aktivnost";
			this.addDoneActivityButton.UseVisualStyleBackColor = true;
			this.addDoneActivityButton.Click += new System.EventHandler(this.addDoneActivityButton_Click);
			// 
			// doneActivitiesListBox
			// 
			this.doneActivitiesListBox.DataSource = this.iDoneActivityBindingSource;
			this.doneActivitiesListBox.DisplayMember = "ListInfo";
			this.doneActivitiesListBox.FormattingEnabled = true;
			this.doneActivitiesListBox.Location = new System.Drawing.Point(12, 204);
			this.doneActivitiesListBox.Name = "doneActivitiesListBox";
			this.doneActivitiesListBox.Size = new System.Drawing.Size(200, 160);
			this.doneActivitiesListBox.TabIndex = 5;
			// 
			// iDoneActivityBindingSource
			// 
			this.iDoneActivityBindingSource.DataSource = typeof(Fitnessify.Model.IDoneActivity);
			// 
			// foodManagementButton
			// 
			this.foodManagementButton.Location = new System.Drawing.Point(218, 126);
			this.foodManagementButton.Name = "foodManagementButton";
			this.foodManagementButton.Size = new System.Drawing.Size(137, 23);
			this.foodManagementButton.TabIndex = 8;
			this.foodManagementButton.Text = "Upravljanje hranom";
			this.foodManagementButton.UseVisualStyleBackColor = true;
			this.foodManagementButton.Click += new System.EventHandler(this.foodManagementButton_Click);
			// 
			// activitiesManagementButton
			// 
			this.activitiesManagementButton.Location = new System.Drawing.Point(218, 289);
			this.activitiesManagementButton.Name = "activitiesManagementButton";
			this.activitiesManagementButton.Size = new System.Drawing.Size(137, 23);
			this.activitiesManagementButton.TabIndex = 9;
			this.activitiesManagementButton.Text = "Upravljanje aktivnostima";
			this.activitiesManagementButton.UseVisualStyleBackColor = true;
			this.activitiesManagementButton.Click += new System.EventHandler(this.activitiesManagementButton_Click);
			// 
			// personalDataButton
			// 
			this.personalDataButton.Location = new System.Drawing.Point(382, 13);
			this.personalDataButton.Name = "personalDataButton";
			this.personalDataButton.Size = new System.Drawing.Size(154, 23);
			this.personalDataButton.TabIndex = 10;
			this.personalDataButton.Text = "Unesi osobne podatke";
			this.personalDataButton.UseVisualStyleBackColor = true;
			this.personalDataButton.Click += new System.EventHandler(this.personalDataButton_Click);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(379, 194);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(77, 13);
			this.label9.TabIndex = 25;
			this.label9.Text = "Vremenski rok:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(379, 168);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(76, 13);
			this.label10.TabIndex = 24;
			this.label10.Text = "Željena težina:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(379, 127);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(31, 13);
			this.label11.TabIndex = 23;
			this.label11.Text = "Spol:";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(379, 100);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(30, 13);
			this.label12.TabIndex = 22;
			this.label12.Text = "Dob:";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(379, 74);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(42, 13);
			this.label13.TabIndex = 21;
			this.label13.Text = "Težina:";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(379, 48);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(38, 13);
			this.label14.TabIndex = 20;
			this.label14.Text = "Visina:";
			// 
			// goalWeightDeadlineLabel
			// 
			this.goalWeightDeadlineLabel.AutoSize = true;
			this.goalWeightDeadlineLabel.Location = new System.Drawing.Point(459, 194);
			this.goalWeightDeadlineLabel.Name = "goalWeightDeadlineLabel";
			this.goalWeightDeadlineLabel.Size = new System.Drawing.Size(10, 13);
			this.goalWeightDeadlineLabel.TabIndex = 31;
			this.goalWeightDeadlineLabel.Text = "-";
			// 
			// goalWeightLabel
			// 
			this.goalWeightLabel.AutoSize = true;
			this.goalWeightLabel.Location = new System.Drawing.Point(459, 168);
			this.goalWeightLabel.Name = "goalWeightLabel";
			this.goalWeightLabel.Size = new System.Drawing.Size(10, 13);
			this.goalWeightLabel.TabIndex = 30;
			this.goalWeightLabel.Text = "-";
			// 
			// genderLabel
			// 
			this.genderLabel.AutoSize = true;
			this.genderLabel.Location = new System.Drawing.Point(423, 127);
			this.genderLabel.Name = "genderLabel";
			this.genderLabel.Size = new System.Drawing.Size(10, 13);
			this.genderLabel.TabIndex = 29;
			this.genderLabel.Text = "-";
			// 
			// ageLabel
			// 
			this.ageLabel.AutoSize = true;
			this.ageLabel.Location = new System.Drawing.Point(423, 100);
			this.ageLabel.Name = "ageLabel";
			this.ageLabel.Size = new System.Drawing.Size(10, 13);
			this.ageLabel.TabIndex = 28;
			this.ageLabel.Text = "-";
			// 
			// weightLabel
			// 
			this.weightLabel.AutoSize = true;
			this.weightLabel.Location = new System.Drawing.Point(423, 74);
			this.weightLabel.Name = "weightLabel";
			this.weightLabel.Size = new System.Drawing.Size(10, 13);
			this.weightLabel.TabIndex = 27;
			this.weightLabel.Text = "-";
			// 
			// heightLabel
			// 
			this.heightLabel.AutoSize = true;
			this.heightLabel.Location = new System.Drawing.Point(423, 48);
			this.heightLabel.Name = "heightLabel";
			this.heightLabel.Size = new System.Drawing.Size(10, 13);
			this.heightLabel.TabIndex = 26;
			this.heightLabel.Text = "-";
			// 
			// basalMetabolismLabel
			// 
			this.basalMetabolismLabel.AutoSize = true;
			this.basalMetabolismLabel.Location = new System.Drawing.Point(495, 259);
			this.basalMetabolismLabel.Name = "basalMetabolismLabel";
			this.basalMetabolismLabel.Size = new System.Drawing.Size(10, 13);
			this.basalMetabolismLabel.TabIndex = 35;
			this.basalMetabolismLabel.Text = "-";
			// 
			// bmiLabel
			// 
			this.bmiLabel.AutoSize = true;
			this.bmiLabel.Location = new System.Drawing.Point(495, 233);
			this.bmiLabel.Name = "bmiLabel";
			this.bmiLabel.Size = new System.Drawing.Size(10, 13);
			this.bmiLabel.TabIndex = 34;
			this.bmiLabel.Text = "-";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(380, 259);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(105, 13);
			this.label3.TabIndex = 33;
			this.label3.Text = "Bazalni metabolizam:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(380, 233);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(109, 13);
			this.label4.TabIndex = 32;
			this.label4.Text = "Indeks tjelesne mase:";
			// 
			// currentDayDeficitLabel
			// 
			this.currentDayDeficitLabel.AutoSize = true;
			this.currentDayDeficitLabel.Location = new System.Drawing.Point(495, 320);
			this.currentDayDeficitLabel.Name = "currentDayDeficitLabel";
			this.currentDayDeficitLabel.Size = new System.Drawing.Size(10, 13);
			this.currentDayDeficitLabel.TabIndex = 39;
			this.currentDayDeficitLabel.Text = "-";
			// 
			// dailyCaloricDeficitLabel
			// 
			this.dailyCaloricDeficitLabel.AutoSize = true;
			this.dailyCaloricDeficitLabel.Location = new System.Drawing.Point(495, 294);
			this.dailyCaloricDeficitLabel.Name = "dailyCaloricDeficitLabel";
			this.dailyCaloricDeficitLabel.Size = new System.Drawing.Size(10, 13);
			this.dailyCaloricDeficitLabel.TabIndex = 38;
			this.dailyCaloricDeficitLabel.Text = "-";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(380, 320);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(111, 13);
			this.label7.TabIndex = 37;
			this.label7.Text = "Potrebno za ovaj dan:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(380, 294);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(103, 13);
			this.label8.TabIndex = 36;
			this.label8.Text = "Ciljani dnevni deficit:";
			// 
			// carbsPercLabel
			// 
			this.carbsPercLabel.AutoSize = true;
			this.carbsPercLabel.Location = new System.Drawing.Point(306, 401);
			this.carbsPercLabel.Name = "carbsPercLabel";
			this.carbsPercLabel.Size = new System.Drawing.Size(10, 13);
			this.carbsPercLabel.TabIndex = 59;
			this.carbsPercLabel.Text = "-";
			// 
			// proteinsPercLabel
			// 
			this.proteinsPercLabel.AutoSize = true;
			this.proteinsPercLabel.Location = new System.Drawing.Point(182, 401);
			this.proteinsPercLabel.Name = "proteinsPercLabel";
			this.proteinsPercLabel.Size = new System.Drawing.Size(10, 13);
			this.proteinsPercLabel.TabIndex = 57;
			this.proteinsPercLabel.Text = "-";
			// 
			// carbsLabel
			// 
			this.carbsLabel.AutoSize = true;
			this.carbsLabel.Location = new System.Drawing.Point(306, 382);
			this.carbsLabel.Name = "carbsLabel";
			this.carbsLabel.Size = new System.Drawing.Size(10, 13);
			this.carbsLabel.TabIndex = 55;
			this.carbsLabel.Text = "-";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(230, 382);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(70, 13);
			this.label16.TabIndex = 54;
			this.label16.Text = "Ugljikohidrati:";
			// 
			// proteinsLabel
			// 
			this.proteinsLabel.AutoSize = true;
			this.proteinsLabel.Location = new System.Drawing.Point(182, 382);
			this.proteinsLabel.Name = "proteinsLabel";
			this.proteinsLabel.Size = new System.Drawing.Size(10, 13);
			this.proteinsLabel.TabIndex = 53;
			this.proteinsLabel.Text = "-";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(105, 382);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(71, 13);
			this.label6.TabIndex = 52;
			this.label6.Text = "Bjelančevine:";
			// 
			// caloriesLabel
			// 
			this.caloriesLabel.AutoSize = true;
			this.caloriesLabel.Location = new System.Drawing.Point(56, 382);
			this.caloriesLabel.Name = "caloriesLabel";
			this.caloriesLabel.Size = new System.Drawing.Size(10, 13);
			this.caloriesLabel.TabIndex = 51;
			this.caloriesLabel.Text = "-";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 382);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(44, 13);
			this.label2.TabIndex = 50;
			this.label2.Text = "Kalorije:";
			// 
			// fatsPercLabel
			// 
			this.fatsPercLabel.AutoSize = true;
			this.fatsPercLabel.Location = new System.Drawing.Point(415, 401);
			this.fatsPercLabel.Name = "fatsPercLabel";
			this.fatsPercLabel.Size = new System.Drawing.Size(10, 13);
			this.fatsPercLabel.TabIndex = 63;
			this.fatsPercLabel.Text = "-";
			// 
			// fatsLabel
			// 
			this.fatsLabel.AutoSize = true;
			this.fatsLabel.Location = new System.Drawing.Point(415, 382);
			this.fatsLabel.Name = "fatsLabel";
			this.fatsLabel.Size = new System.Drawing.Size(10, 13);
			this.fatsLabel.TabIndex = 61;
			this.fatsLabel.Text = "-";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(374, 382);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(35, 13);
			this.label24.TabIndex = 60;
			this.label24.Text = "Masti:";
			// 
			// DayView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(546, 443);
			this.Controls.Add(this.fatsPercLabel);
			this.Controls.Add(this.fatsLabel);
			this.Controls.Add(this.label24);
			this.Controls.Add(this.carbsPercLabel);
			this.Controls.Add(this.proteinsPercLabel);
			this.Controls.Add(this.carbsLabel);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.proteinsLabel);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.caloriesLabel);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.currentDayDeficitLabel);
			this.Controls.Add(this.dailyCaloricDeficitLabel);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.basalMetabolismLabel);
			this.Controls.Add(this.bmiLabel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.goalWeightDeadlineLabel);
			this.Controls.Add(this.goalWeightLabel);
			this.Controls.Add(this.genderLabel);
			this.Controls.Add(this.ageLabel);
			this.Controls.Add(this.weightLabel);
			this.Controls.Add(this.heightLabel);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.personalDataButton);
			this.Controls.Add(this.activitiesManagementButton);
			this.Controls.Add(this.foodManagementButton);
			this.Controls.Add(this.deleteDoneActivityButton);
			this.Controls.Add(this.addDoneActivityButton);
			this.Controls.Add(this.doneActivitiesListBox);
			this.Controls.Add(this.deleteConsumedFoodButton);
			this.Controls.Add(this.AddConsumedFoodButton);
			this.Controls.Add(this.consumedFoodsListBox);
			this.Controls.Add(this.dateTimePicker1);
			this.Name = "DayView";
			this.Text = "Pregled dana";
			((System.ComponentModel.ISupportInitialize)(this.consumedFoodBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.iDoneActivityBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

		#endregion

		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.ListBox consumedFoodsListBox;
		private System.Windows.Forms.BindingSource consumedFoodBindingSource;
		private System.Windows.Forms.Button AddConsumedFoodButton;
		private System.Windows.Forms.Button deleteConsumedFoodButton;
		private System.Windows.Forms.Button deleteDoneActivityButton;
		private System.Windows.Forms.Button addDoneActivityButton;
		private System.Windows.Forms.ListBox doneActivitiesListBox;
		private System.Windows.Forms.BindingSource iDoneActivityBindingSource;
		private System.Windows.Forms.Button foodManagementButton;
		private System.Windows.Forms.Button activitiesManagementButton;
		private System.Windows.Forms.Button personalDataButton;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label goalWeightDeadlineLabel;
		private System.Windows.Forms.Label goalWeightLabel;
		private System.Windows.Forms.Label genderLabel;
		private System.Windows.Forms.Label ageLabel;
		private System.Windows.Forms.Label weightLabel;
		private System.Windows.Forms.Label heightLabel;
		private System.Windows.Forms.Label basalMetabolismLabel;
		private System.Windows.Forms.Label bmiLabel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label currentDayDeficitLabel;
		private System.Windows.Forms.Label dailyCaloricDeficitLabel;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label carbsPercLabel;
		private System.Windows.Forms.Label proteinsPercLabel;
		private System.Windows.Forms.Label carbsLabel;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label proteinsLabel;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label caloriesLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label fatsPercLabel;
		private System.Windows.Forms.Label fatsLabel;
		private System.Windows.Forms.Label label24;
	}
}

