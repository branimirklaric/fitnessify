﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class PersonalDataDialog : Form {
		internal PersonalData PersonalData { get; set; }

		public PersonalDataDialog(PersonalData personalData) {
			InitializeComponent();
			PersonalData = personalData;
			if (personalData == null) {
				return;
			}
			heightNumeric.Value = personalData.Height;
			weightNumeric.Value = personalData.Weight;
			ageNumeric.Value = personalData.Age;
			genderCombo.SelectedIndex = personalData.Gender == Gender.Male ? 0 : 1;
			goalWeightNumeric.Value = (decimal)personalData.GoalWeight;
			goalWeightDeadlineNumeric.Value = (decimal)personalData.GoalWeightDeadline;
        }

		private void saveDataButton_Click(object sender, EventArgs e) {
			var height = (int)heightNumeric.Value;
			var weight = weightNumeric.Value;
			var age = (int)ageNumeric.Value;
			var gender = genderCombo.Text == "M" ? Gender.Male : Gender.Female;
			var goalWeight = (int)goalWeightNumeric.Value;
			var goalWeightDeadline = (int)goalWeightDeadlineNumeric.Value;
			PersonalData = new PersonalData(height, weight, age, gender, 
				goalWeight, goalWeightDeadline);
			DialogResult = DialogResult.OK;
			Close();
		}

		private void cancelButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
