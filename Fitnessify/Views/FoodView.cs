﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class FoodView : Form {
		FoodController foodController;
		IList<Foodstuff> allFoodstuffs;
		IList<Recipe> allRecipes;

		public FoodView(FoodController foodController, IList<Foodstuff> allFoodstuffs,
				IList<Recipe> allRecipes) {
			InitializeComponent();
			this.foodController = foodController;
			this.allFoodstuffs = allFoodstuffs;
			this.allRecipes = allRecipes;
			foodstuffBindingSource.DataSource = this.allFoodstuffs;
			recipeBindingSource.DataSource = this.allRecipes;
		}

		public void UpdateView(IList<Foodstuff> allFoodstuffs) {
			this.allFoodstuffs = allFoodstuffs;
			foodstuffBindingSource.DataSource = this.allFoodstuffs;
			foodstuffBindingSource.ResetBindings(false);
		}

		public void UpdateView(IList<Recipe> allRecipes) {
			this.allRecipes = allRecipes;
			recipeBindingSource.DataSource = this.allRecipes;
			recipeBindingSource.ResetBindings(false);
		}

		private void addNewFoodstuffButton_Click(object sender, EventArgs e) {
			foodController.OpenAddFoodstuffDialog();
		}

		private void editFoodstuffButton_Click(object sender, EventArgs e) {
			foodController.OpenEditFoodstuffDialog(
				foodstuffBindingSource.Current as Foodstuff);
		}

		private void deleteFoodstuffButton_Click(object sender, EventArgs e) {
			foodController.DeleteFoodstuff(foodstuffBindingSource.Current as Foodstuff);
		}

		private void addRecipeButton_Click(object sender, EventArgs e) {
			foodController.OpenAddRecipeDialog();
		}

		private void editRecipeButton_Click(object sender, EventArgs e) {
			foodController.OpenEditRecipeDialog(recipeBindingSource.Current as Recipe);
		}

		private void deleteRecipeButton_Click(object sender, EventArgs e) {
			foodController.DeleteRecipe(recipeBindingSource.Current as Recipe);
		}
	}
}
