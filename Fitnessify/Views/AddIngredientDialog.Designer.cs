﻿namespace Fitnessify {
	partial class AddIngredientDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.foodstuffBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.GiveUpButton = new System.Windows.Forms.Button();
			this.AddButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.foodstuffBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(13, 40);
			this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(144, 20);
			this.numericUpDown1.TabIndex = 7;
			this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// comboBox1
			// 
			this.comboBox1.DataSource = this.foodstuffBindingSource;
			this.comboBox1.DisplayMember = "ListInfo";
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(12, 12);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(145, 21);
			this.comboBox1.TabIndex = 6;
			// 
			// foodstuffBindingSource
			// 
			this.foodstuffBindingSource.DataSource = typeof(Fitnessify.Model.Foodstuff);
			// 
			// GiveUpButton
			// 
			this.GiveUpButton.Location = new System.Drawing.Point(13, 94);
			this.GiveUpButton.Name = "GiveUpButton";
			this.GiveUpButton.Size = new System.Drawing.Size(144, 23);
			this.GiveUpButton.TabIndex = 5;
			this.GiveUpButton.Text = "Odustani";
			this.GiveUpButton.UseVisualStyleBackColor = true;
			this.GiveUpButton.Click += new System.EventHandler(this.GiveUpButton_Click_1);
			// 
			// AddButton
			// 
			this.AddButton.Location = new System.Drawing.Point(13, 65);
			this.AddButton.Name = "AddButton";
			this.AddButton.Size = new System.Drawing.Size(144, 23);
			this.AddButton.TabIndex = 4;
			this.AddButton.Text = "Dodaj";
			this.AddButton.UseVisualStyleBackColor = true;
			this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// AddIngredientDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(170, 129);
			this.Controls.Add(this.numericUpDown1);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.GiveUpButton);
			this.Controls.Add(this.AddButton);
			this.Name = "AddIngredientDialog";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.foodstuffBindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button GiveUpButton;
		private System.Windows.Forms.Button AddButton;
		private System.Windows.Forms.BindingSource foodstuffBindingSource;
	}
}