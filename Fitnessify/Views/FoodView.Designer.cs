﻿namespace Fitnessify {
	partial class FoodView {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.caloriesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.proteinsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.carbsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fatsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.waterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.foodstuffBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.addNewFoodstuffButton = new System.Windows.Forms.Button();
			this.deleteFoodstuffButton = new System.Windows.Forms.Button();
			this.editFoodstuffButton = new System.Windows.Forms.Button();
			this.dataGridView2 = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.recipeBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.editRecipeButton = new System.Windows.Forms.Button();
			this.deleteRecipeButton = new System.Windows.Forms.Button();
			this.addRecipeButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.foodstuffBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.recipeBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AutoGenerateColumns = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.caloriesDataGridViewTextBoxColumn,
            this.proteinsDataGridViewTextBoxColumn,
            this.carbsDataGridViewTextBoxColumn,
            this.fatsDataGridViewTextBoxColumn,
            this.waterDataGridViewTextBoxColumn});
			this.dataGridView1.DataSource = this.foodstuffBindingSource;
			this.dataGridView1.Location = new System.Drawing.Point(12, 12);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.Size = new System.Drawing.Size(646, 146);
			this.dataGridView1.TabIndex = 0;
			// 
			// nameDataGridViewTextBoxColumn
			// 
			this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
			this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
			this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
			this.nameDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// caloriesDataGridViewTextBoxColumn
			// 
			this.caloriesDataGridViewTextBoxColumn.DataPropertyName = "Calories";
			this.caloriesDataGridViewTextBoxColumn.HeaderText = "Calories";
			this.caloriesDataGridViewTextBoxColumn.Name = "caloriesDataGridViewTextBoxColumn";
			this.caloriesDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// proteinsDataGridViewTextBoxColumn
			// 
			this.proteinsDataGridViewTextBoxColumn.DataPropertyName = "Proteins";
			this.proteinsDataGridViewTextBoxColumn.HeaderText = "Proteins";
			this.proteinsDataGridViewTextBoxColumn.Name = "proteinsDataGridViewTextBoxColumn";
			this.proteinsDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// carbsDataGridViewTextBoxColumn
			// 
			this.carbsDataGridViewTextBoxColumn.DataPropertyName = "Carbs";
			this.carbsDataGridViewTextBoxColumn.HeaderText = "Carbs";
			this.carbsDataGridViewTextBoxColumn.Name = "carbsDataGridViewTextBoxColumn";
			this.carbsDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// fatsDataGridViewTextBoxColumn
			// 
			this.fatsDataGridViewTextBoxColumn.DataPropertyName = "Fats";
			this.fatsDataGridViewTextBoxColumn.HeaderText = "Fats";
			this.fatsDataGridViewTextBoxColumn.Name = "fatsDataGridViewTextBoxColumn";
			this.fatsDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// waterDataGridViewTextBoxColumn
			// 
			this.waterDataGridViewTextBoxColumn.DataPropertyName = "Water";
			this.waterDataGridViewTextBoxColumn.HeaderText = "Water";
			this.waterDataGridViewTextBoxColumn.Name = "waterDataGridViewTextBoxColumn";
			this.waterDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// foodstuffBindingSource
			// 
			this.foodstuffBindingSource.DataSource = typeof(Fitnessify.Model.Foodstuff);
			// 
			// addNewFoodstuffButton
			// 
			this.addNewFoodstuffButton.Location = new System.Drawing.Point(12, 164);
			this.addNewFoodstuffButton.Name = "addNewFoodstuffButton";
			this.addNewFoodstuffButton.Size = new System.Drawing.Size(119, 23);
			this.addNewFoodstuffButton.TabIndex = 1;
			this.addNewFoodstuffButton.Text = "Dodaj novu namirnicu";
			this.addNewFoodstuffButton.UseVisualStyleBackColor = true;
			this.addNewFoodstuffButton.Click += new System.EventHandler(this.addNewFoodstuffButton_Click);
			// 
			// deleteFoodstuffButton
			// 
			this.deleteFoodstuffButton.Location = new System.Drawing.Point(268, 164);
			this.deleteFoodstuffButton.Name = "deleteFoodstuffButton";
			this.deleteFoodstuffButton.Size = new System.Drawing.Size(123, 23);
			this.deleteFoodstuffButton.TabIndex = 2;
			this.deleteFoodstuffButton.Text = "Izbriši namirnicu";
			this.deleteFoodstuffButton.UseVisualStyleBackColor = true;
			this.deleteFoodstuffButton.Click += new System.EventHandler(this.deleteFoodstuffButton_Click);
			// 
			// editFoodstuffButton
			// 
			this.editFoodstuffButton.Location = new System.Drawing.Point(137, 164);
			this.editFoodstuffButton.Name = "editFoodstuffButton";
			this.editFoodstuffButton.Size = new System.Drawing.Size(125, 23);
			this.editFoodstuffButton.TabIndex = 4;
			this.editFoodstuffButton.Text = "Uredi namirnicu";
			this.editFoodstuffButton.UseVisualStyleBackColor = true;
			this.editFoodstuffButton.Click += new System.EventHandler(this.editFoodstuffButton_Click);
			// 
			// dataGridView2
			// 
			this.dataGridView2.AllowUserToAddRows = false;
			this.dataGridView2.AllowUserToDeleteRows = false;
			this.dataGridView2.AutoGenerateColumns = false;
			this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
			this.dataGridView2.DataSource = this.recipeBindingSource;
			this.dataGridView2.Location = new System.Drawing.Point(12, 215);
			this.dataGridView2.Name = "dataGridView2";
			this.dataGridView2.ReadOnly = true;
			this.dataGridView2.Size = new System.Drawing.Size(646, 146);
			this.dataGridView2.TabIndex = 13;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
			this.dataGridViewTextBoxColumn1.HeaderText = "Name";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.DataPropertyName = "Calories";
			this.dataGridViewTextBoxColumn2.HeaderText = "Calories";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.DataPropertyName = "Proteins";
			this.dataGridViewTextBoxColumn3.HeaderText = "Proteins";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.DataPropertyName = "Carbs";
			this.dataGridViewTextBoxColumn4.HeaderText = "Carbs";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn5
			// 
			this.dataGridViewTextBoxColumn5.DataPropertyName = "Fats";
			this.dataGridViewTextBoxColumn5.HeaderText = "Fats";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			// 
			// dataGridViewTextBoxColumn6
			// 
			this.dataGridViewTextBoxColumn6.DataPropertyName = "Water";
			this.dataGridViewTextBoxColumn6.HeaderText = "Water";
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			this.dataGridViewTextBoxColumn6.ReadOnly = true;
			// 
			// recipeBindingSource
			// 
			this.recipeBindingSource.DataSource = typeof(Fitnessify.Model.Recipe);
			// 
			// editRecipeButton
			// 
			this.editRecipeButton.Location = new System.Drawing.Point(137, 367);
			this.editRecipeButton.Name = "editRecipeButton";
			this.editRecipeButton.Size = new System.Drawing.Size(125, 23);
			this.editRecipeButton.TabIndex = 17;
			this.editRecipeButton.Text = "Uredi recept";
			this.editRecipeButton.UseVisualStyleBackColor = true;
			this.editRecipeButton.Click += new System.EventHandler(this.editRecipeButton_Click);
			// 
			// deleteRecipeButton
			// 
			this.deleteRecipeButton.Location = new System.Drawing.Point(268, 367);
			this.deleteRecipeButton.Name = "deleteRecipeButton";
			this.deleteRecipeButton.Size = new System.Drawing.Size(123, 23);
			this.deleteRecipeButton.TabIndex = 15;
			this.deleteRecipeButton.Text = "Izbriši recept";
			this.deleteRecipeButton.UseVisualStyleBackColor = true;
			this.deleteRecipeButton.Click += new System.EventHandler(this.deleteRecipeButton_Click);
			// 
			// addRecipeButton
			// 
			this.addRecipeButton.Location = new System.Drawing.Point(12, 367);
			this.addRecipeButton.Name = "addRecipeButton";
			this.addRecipeButton.Size = new System.Drawing.Size(119, 23);
			this.addRecipeButton.TabIndex = 14;
			this.addRecipeButton.Text = "Dodaj novi recept";
			this.addRecipeButton.UseVisualStyleBackColor = true;
			this.addRecipeButton.Click += new System.EventHandler(this.addRecipeButton_Click);
			// 
			// FoodView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(670, 415);
			this.Controls.Add(this.editRecipeButton);
			this.Controls.Add(this.deleteRecipeButton);
			this.Controls.Add(this.addRecipeButton);
			this.Controls.Add(this.dataGridView2);
			this.Controls.Add(this.editFoodstuffButton);
			this.Controls.Add(this.deleteFoodstuffButton);
			this.Controls.Add(this.addNewFoodstuffButton);
			this.Controls.Add(this.dataGridView1);
			this.Name = "FoodView";
			this.Text = "Pregled hrane";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.foodstuffBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.recipeBindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.BindingSource foodstuffBindingSource;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn caloriesDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn proteinsDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn carbsDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn fatsDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn waterDataGridViewTextBoxColumn;
		private System.Windows.Forms.Button addNewFoodstuffButton;
		private System.Windows.Forms.Button deleteFoodstuffButton;
		private System.Windows.Forms.Button editFoodstuffButton;
		private System.Windows.Forms.BindingSource recipeBindingSource;
		private System.Windows.Forms.DataGridView dataGridView2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
		private System.Windows.Forms.Button editRecipeButton;
		private System.Windows.Forms.Button deleteRecipeButton;
		private System.Windows.Forms.Button addRecipeButton;
	}
}