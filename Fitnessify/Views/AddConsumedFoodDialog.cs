﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class AddConsumedFoodDialog : Form {
		public ConsumedFood ConsumedFood { get; set; }

		public AddConsumedFoodDialog(IList<Food> allFoods) {
			InitializeComponent();
			foodBindingSource.DataSource = allFoods;
		}

		private void AddButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.OK;
			ConsumedFood = new ConsumedFood(
				foodBindingSource.Current as Food, (int)numericUpDown1.Value);
			Close();
		}

		private void GiveUpButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
