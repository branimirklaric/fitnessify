﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class AddDoneActivityDialog : Form {
		public IDoneActivity DoneActivity { get; set; }

		public AddDoneActivityDialog(IList<PhysicalActivity> allActivities) {
			InitializeComponent();
			physicalActivityBindingSource.DataSource = allActivities;
		}

		private void AddButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.OK;
			var activityAsExercise = physicalActivityBindingSource.Current as Exercise;
			if (activityAsExercise != null) {
				DoneActivity = new DoneExercise(
					activityAsExercise, (int)numericUpDown1.Value);
			}
			else {
				DoneActivity = new DoneRoutine(
					physicalActivityBindingSource.Current as Routine, 
					(int)numericUpDown1.Value);
			}
			Close();
		}

		private void GiveUpButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
