﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class AddOrEditRecipeDialog : Form {
		RecipeController recipeController;
		internal Recipe Recipe { get; set; }

		internal AddOrEditRecipeDialog(RecipeController recipeController, 
				Recipe recipe) {
			InitializeComponent();
			this.recipeController = recipeController;
			Recipe = recipe;
			nameTextBox.Text = Recipe.Name;
			ingredientBindingSource.DataSource = Recipe.Ingredients;
		}

		internal void RefreshRecipe(Recipe recipe) {
			Recipe = recipe;
			nameTextBox.Text = Recipe.Name;
			ingredientBindingSource.DataSource = Recipe.Ingredients;
			ingredientBindingSource.ResetBindings(false);
		}

		private void addIngredientButton_Click(object sender, EventArgs e) {
			recipeController.OpenAddIngredientDialog();
		}

		private void deleteIngredientButton_Click(object sender, EventArgs e) {
			recipeController.DeleteIngredient(
				ingredientBindingSource.Current as Ingredient);
        }

		private void okButton_Click(object sender, EventArgs e) {
			if (recipeController.NameNotUnique(nameTextBox.Text)) {
				MessageBox.Show("Već postoji recept s tim imenom.");
				return;
			}
			try {
				Recipe.Name = nameTextBox.Text;
			}
			catch {
				MessageBox.Show("Neispravno ime.");
				return;
			}
			DialogResult = DialogResult.OK;
			Close();
		}

		private void cancelButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
