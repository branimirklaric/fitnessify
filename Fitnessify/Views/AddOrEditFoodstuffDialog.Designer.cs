﻿namespace Fitnessify {
	partial class AddOrEditFoodstuffDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.proteinsNumeric = new System.Windows.Forms.NumericUpDown();
			this.carbsNumerc = new System.Windows.Forms.NumericUpDown();
			this.fatsNumeric = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.nameTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.proteinsNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.carbsNumerc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fatsNumeric)).BeginInit();
			this.SuspendLayout();
			// 
			// proteinsNumeric
			// 
			this.proteinsNumeric.Location = new System.Drawing.Point(90, 38);
			this.proteinsNumeric.Name = "proteinsNumeric";
			this.proteinsNumeric.Size = new System.Drawing.Size(120, 20);
			this.proteinsNumeric.TabIndex = 0;
			// 
			// carbsNumerc
			// 
			this.carbsNumerc.Location = new System.Drawing.Point(90, 64);
			this.carbsNumerc.Name = "carbsNumerc";
			this.carbsNumerc.Size = new System.Drawing.Size(120, 20);
			this.carbsNumerc.TabIndex = 1;
			// 
			// fatsNumeric
			// 
			this.fatsNumeric.Location = new System.Drawing.Point(90, 90);
			this.fatsNumeric.Name = "fatsNumeric";
			this.fatsNumeric.Size = new System.Drawing.Size(120, 20);
			this.fatsNumeric.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(14, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Proteini:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(14, 66);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Ugljikohidrati:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 92);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(35, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Masti:";
			// 
			// okButton
			// 
			this.okButton.Location = new System.Drawing.Point(14, 134);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(93, 23);
			this.okButton.TabIndex = 6;
			this.okButton.Text = "Spremi";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.Location = new System.Drawing.Point(113, 134);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(97, 23);
			this.cancelButton.TabIndex = 8;
			this.cancelButton.Text = "Odustani";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// nameTextBox
			// 
			this.nameTextBox.Location = new System.Drawing.Point(90, 12);
			this.nameTextBox.Name = "nameTextBox";
			this.nameTextBox.Size = new System.Drawing.Size(120, 20);
			this.nameTextBox.TabIndex = 9;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(14, 15);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(27, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Ime:";
			// 
			// AddOrEditFoodstuffDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(222, 173);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.nameTextBox);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.fatsNumeric);
			this.Controls.Add(this.carbsNumerc);
			this.Controls.Add(this.proteinsNumeric);
			this.Name = "AddOrEditFoodstuffDialog";
			this.Text = "Uredi namirnicu";
			((System.ComponentModel.ISupportInitialize)(this.proteinsNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.carbsNumerc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fatsNumeric)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.NumericUpDown proteinsNumeric;
		private System.Windows.Forms.NumericUpDown carbsNumerc;
		private System.Windows.Forms.NumericUpDown fatsNumeric;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.Label label4;
	}
}