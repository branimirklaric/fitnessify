﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class AddOrEditFoodstuffDialog : Form {
		FoodController foodController;
		internal Foodstuff Foodstuff { get; set; }		

		public AddOrEditFoodstuffDialog(FoodController foodController) {
			InitializeComponent();
			this.foodController = foodController;
			Foodstuff = new Foodstuff("Ime namirnice");
			SetInputBoxes();
		}

		public AddOrEditFoodstuffDialog(
				FoodController foodController, Foodstuff foodstuff) {
			InitializeComponent();
			this.foodController = foodController;
			Foodstuff = foodstuff;
			SetInputBoxes();
		}

		private void SetInputBoxes() {
			nameTextBox.Text = Foodstuff.Name;
			proteinsNumeric.Value = Foodstuff.Proteins;
			carbsNumerc.Value = Foodstuff.Carbs;
			fatsNumeric.Value = Foodstuff.Fats;
		}

		private void okButton_Click(object sender, EventArgs e) {
			var nutritionValues = new Dictionary<Nutrient, int>() {
				{ Nutrient.Proteins, (int)proteinsNumeric.Value },
				{ Nutrient.Carbs, (int)carbsNumerc.Value },
				{ Nutrient.Fats, (int)fatsNumeric.Value }
			};
			try {
				Foodstuff.SetNutritionalValues(nutritionValues);
			}
			catch {
				MessageBox.Show("Neispravne vrijednosti. Zbroj mora biti <= 100.");
				return;
			}

			if (foodController.NameNotUnique(Foodstuff.Id, nameTextBox.Text)) {
				MessageBox.Show("Već postoji namirnica s tim imenom.");
				return;
			}
			try {
				Foodstuff.Name = nameTextBox.Text;
			}
			catch {
				MessageBox.Show("Neispravno ime.");
				return;
			}			
            DialogResult = DialogResult.OK;
			Close();
		}

		private void cancelButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
