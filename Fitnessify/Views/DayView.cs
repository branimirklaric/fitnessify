﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
    public partial class DayView : Form {
		DayController dayController;
		Model.Day selectedDay;
		PersonalData personalData;

		public DayView(DayController dayController, Model.Day selectedDay,
                PersonalData personalData) {
            InitializeComponent();
			this.dayController = dayController;
			this.selectedDay = selectedDay;
			this.personalData = personalData;
			RefreshEverything();
        }

		internal void UpdateDay(Model.Day day) {
			selectedDay = day;
			RefreshEverything();
        }

		void RefreshListBoxes() {
			consumedFoodBindingSource.DataSource = selectedDay.ConsumedFoods;
			consumedFoodBindingSource.ResetBindings(false);
			iDoneActivityBindingSource.DataSource = selectedDay.DoneActivites;
			iDoneActivityBindingSource.ResetBindings(false);
		}

		internal void UpdatePersonalData(PersonalData personalData) {
			this.personalData = personalData;
			RefreshPersonalData();
		}

		void RefreshPersonalData() {
			if (personalData == null) {
				return;
			}
			heightLabel.Text = personalData.Height.ToString() + " cm";
			weightLabel.Text = personalData.Weight.ToString() + " kg";
			ageLabel.Text = personalData.Age.ToString();
			genderLabel.Text = personalData.Gender == Gender.Male ? "M" : "Ž";

			goalWeightLabel.Text = personalData.GoalWeight.ToString() + " kg";
			goalWeightDeadlineLabel.Text = 
				personalData.GoalWeightDeadline.ToString() + " dana";

			bmiLabel.Text = personalData.Bmi().ToString().Substring(0, 4);
			basalMetabolismLabel.Text = personalData.BasalMetabolism().ToString();

			dailyCaloricDeficitLabel.Text = 
				personalData.DailyCaloricDeficit().ToString();
			currentDayDeficitLabel.Text = (dayController.CaloricBalance() 
				+ personalData.DailyCaloricDeficit() - personalData.BasalMetabolism())
				.ToString();

		}

		void RefreshNutritionInfo() {
			caloriesLabel.Text = dayController.Calories.ToString();
			proteinsLabel.Text = dayController.Proteins.ToString();
			carbsLabel.Text = dayController.Carbs.ToString();
			fatsLabel.Text = dayController.Fats.ToString();
			proteinsPercLabel.Text = dayController.ProteinsPercentage
				.ToString().Substring(0, 4) + "%";
			carbsPercLabel.Text = dayController.CarbsPercentage
				.ToString().Substring(0, 4) + "%";
			fatsPercLabel.Text = dayController.FatsPercentage
				.ToString().Substring(0, 4) + "%";
		}

		void RefreshEverything() {
			RefreshListBoxes();
			RefreshPersonalData();
			RefreshNutritionInfo();
        }

		private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
			selectedDay = dayController.GetDay((sender as DateTimePicker).Value.Date);
			RefreshEverything();
        }

		private void AddConsumedFoodButton_Click(object sender, EventArgs e) {
			dayController.OpenAddConsumedFoodDialog();
		}

		private void deleteConsumedFoodButton_Click(object sender, EventArgs e) {
			dayController.DeleteConsumedFood(
				consumedFoodBindingSource.Current as ConsumedFood);
		}

		private void addDoneActivityButton_Click(object sender, EventArgs e) {
			dayController.OpenAddDoneActivityDialog();
		}

		private void deleteDoneActivityButton_Click(object sender, EventArgs e) {
			dayController.DeleteDoneActivity(
				iDoneActivityBindingSource.Current as IDoneActivity);
		}

		private void foodManagementButton_Click(object sender, EventArgs e) {
			dayController.OpenFoodManagementView();
		}

		private void activitiesManagementButton_Click(object sender, EventArgs e) {
			dayController.OpenActivitiesManagementView();
		}

		private void personalDataButton_Click(object sender, EventArgs e) {
			dayController.OpenPersonalDataDialog();
		}
	}
}
