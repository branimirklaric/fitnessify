﻿namespace Fitnessify {
	partial class AddOrEditRecipeDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.ingredientBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.deleteIngredientButton = new System.Windows.Forms.Button();
			this.addIngredientButton = new System.Windows.Forms.Button();
			this.nameTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cancelButton = new System.Windows.Forms.Button();
			this.okButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ingredientBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// listBox1
			// 
			this.listBox1.DataSource = this.ingredientBindingSource;
			this.listBox1.DisplayMember = "ListInfo";
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(12, 41);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(202, 134);
			this.listBox1.TabIndex = 0;
			// 
			// ingredientBindingSource
			// 
			this.ingredientBindingSource.DataSource = typeof(Fitnessify.Model.Ingredient);
			// 
			// deleteIngredientButton
			// 
			this.deleteIngredientButton.Location = new System.Drawing.Point(116, 189);
			this.deleteIngredientButton.Name = "deleteIngredientButton";
			this.deleteIngredientButton.Size = new System.Drawing.Size(98, 23);
			this.deleteIngredientButton.TabIndex = 6;
			this.deleteIngredientButton.Text = "Obriši sastojak";
			this.deleteIngredientButton.UseVisualStyleBackColor = true;
			this.deleteIngredientButton.Click += new System.EventHandler(this.deleteIngredientButton_Click);
			// 
			// addIngredientButton
			// 
			this.addIngredientButton.Location = new System.Drawing.Point(12, 189);
			this.addIngredientButton.Name = "addIngredientButton";
			this.addIngredientButton.Size = new System.Drawing.Size(98, 23);
			this.addIngredientButton.TabIndex = 5;
			this.addIngredientButton.Text = "Dodaj sastojak";
			this.addIngredientButton.UseVisualStyleBackColor = true;
			this.addIngredientButton.Click += new System.EventHandler(this.addIngredientButton_Click);
			// 
			// nameTextBox
			// 
			this.nameTextBox.Location = new System.Drawing.Point(45, 12);
			this.nameTextBox.Name = "nameTextBox";
			this.nameTextBox.Size = new System.Drawing.Size(169, 20);
			this.nameTextBox.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(27, 13);
			this.label1.TabIndex = 8;
			this.label1.Text = "Ime:";
			// 
			// cancelButton
			// 
			this.cancelButton.Location = new System.Drawing.Point(116, 218);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(98, 23);
			this.cancelButton.TabIndex = 10;
			this.cancelButton.Text = "Odustani";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// okButton
			// 
			this.okButton.Location = new System.Drawing.Point(12, 218);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(98, 23);
			this.okButton.TabIndex = 9;
			this.okButton.Text = "Spremi";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// AddOrEditRecipeDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(225, 248);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.nameTextBox);
			this.Controls.Add(this.deleteIngredientButton);
			this.Controls.Add(this.addIngredientButton);
			this.Controls.Add(this.listBox1);
			this.Name = "AddOrEditRecipeDialog";
			this.Text = "Uredi recept";
			((System.ComponentModel.ISupportInitialize)(this.ingredientBindingSource)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.BindingSource ingredientBindingSource;
		private System.Windows.Forms.Button deleteIngredientButton;
		private System.Windows.Forms.Button addIngredientButton;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button okButton;
	}
}