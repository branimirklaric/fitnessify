﻿namespace Fitnessify {
	partial class PersonalDataDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.label1 = new System.Windows.Forms.Label();
			this.heightNumeric = new System.Windows.Forms.NumericUpDown();
			this.weightNumeric = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.ageNumeric = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.genderCombo = new System.Windows.Forms.ComboBox();
			this.goalWeightNumeric = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.goalWeightDeadlineNumeric = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.saveDataButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.heightNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.weightNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ageNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.goalWeightNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.goalWeightDeadlineNumeric)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Visina:";
			// 
			// heightNumeric
			// 
			this.heightNumeric.Location = new System.Drawing.Point(56, 15);
			this.heightNumeric.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
			this.heightNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.heightNumeric.Name = "heightNumeric";
			this.heightNumeric.Size = new System.Drawing.Size(120, 20);
			this.heightNumeric.TabIndex = 1;
			this.heightNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// weightNumeric
			// 
			this.weightNumeric.Location = new System.Drawing.Point(56, 41);
			this.weightNumeric.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.weightNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.weightNumeric.Name = "weightNumeric";
			this.weightNumeric.Size = new System.Drawing.Size(120, 20);
			this.weightNumeric.TabIndex = 3;
			this.weightNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 43);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(42, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Težina:";
			// 
			// ageNumeric
			// 
			this.ageNumeric.Location = new System.Drawing.Point(56, 67);
			this.ageNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.ageNumeric.Name = "ageNumeric";
			this.ageNumeric.Size = new System.Drawing.Size(120, 20);
			this.ageNumeric.TabIndex = 5;
			this.ageNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 69);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(30, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Dob:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(31, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Spol:";
			// 
			// genderCombo
			// 
			this.genderCombo.FormattingEnabled = true;
			this.genderCombo.Items.AddRange(new object[] {
            "M",
            "Ž"});
			this.genderCombo.Location = new System.Drawing.Point(55, 93);
			this.genderCombo.Name = "genderCombo";
			this.genderCombo.Size = new System.Drawing.Size(121, 21);
			this.genderCombo.TabIndex = 8;
			// 
			// goalWeightNumeric
			// 
			this.goalWeightNumeric.Location = new System.Drawing.Point(94, 135);
			this.goalWeightNumeric.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.goalWeightNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.goalWeightNumeric.Name = "goalWeightNumeric";
			this.goalWeightNumeric.Size = new System.Drawing.Size(120, 20);
			this.goalWeightNumeric.TabIndex = 10;
			this.goalWeightNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 137);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(76, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Željena težina:";
			// 
			// goalWeightDeadlineNumeric
			// 
			this.goalWeightDeadlineNumeric.Location = new System.Drawing.Point(94, 161);
			this.goalWeightDeadlineNumeric.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.goalWeightDeadlineNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.goalWeightDeadlineNumeric.Name = "goalWeightDeadlineNumeric";
			this.goalWeightDeadlineNumeric.Size = new System.Drawing.Size(120, 20);
			this.goalWeightDeadlineNumeric.TabIndex = 12;
			this.goalWeightDeadlineNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 163);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(77, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "Vremenski rok:";
			// 
			// saveDataButton
			// 
			this.saveDataButton.Location = new System.Drawing.Point(12, 199);
			this.saveDataButton.Name = "saveDataButton";
			this.saveDataButton.Size = new System.Drawing.Size(98, 23);
			this.saveDataButton.TabIndex = 13;
			this.saveDataButton.Text = "Spremi";
			this.saveDataButton.UseVisualStyleBackColor = true;
			this.saveDataButton.Click += new System.EventHandler(this.saveDataButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.Location = new System.Drawing.Point(116, 199);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(98, 23);
			this.cancelButton.TabIndex = 14;
			this.cancelButton.Text = "Odustani";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// PersonalDataDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(228, 233);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveDataButton);
			this.Controls.Add(this.goalWeightDeadlineNumeric);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.goalWeightNumeric);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.genderCombo);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.ageNumeric);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.weightNumeric);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.heightNumeric);
			this.Controls.Add(this.label1);
			this.Name = "PersonalDataDialog";
			this.Text = "Osobni podaci";
			((System.ComponentModel.ISupportInitialize)(this.heightNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.weightNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ageNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.goalWeightNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.goalWeightDeadlineNumeric)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown heightNumeric;
		private System.Windows.Forms.NumericUpDown weightNumeric;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown ageNumeric;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox genderCombo;
		private System.Windows.Forms.NumericUpDown goalWeightNumeric;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown goalWeightDeadlineNumeric;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button saveDataButton;
		private System.Windows.Forms.Button cancelButton;
	}
}