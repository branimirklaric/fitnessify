﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public partial class AddIngredientDialog : Form {
		RecipeController recipeController;
		public Ingredient Ingredient { get; set; }

		public AddIngredientDialog(RecipeController recipeController,
				IList<Foodstuff> allFoodstuffs) {
			InitializeComponent();
			this.recipeController = recipeController;
			foodstuffBindingSource.DataSource = allFoodstuffs;
		}

		private void AddButton_Click(object sender, EventArgs e) {
			Ingredient = new Ingredient(
				foodstuffBindingSource.Current as Foodstuff, (int)numericUpDown1.Value);
			if (recipeController.IngredientAlreadyAdded(Ingredient.Foodstuff.Name)) {
				MessageBox.Show("Sastojak već dodan u recept.");
				return;
			}
			DialogResult = DialogResult.OK;
			Close();
		}

		private void GiveUpButton_Click_1(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
