﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public class FoodController {
		IFoodRepository foodRepo;
		FoodView foodView;
		IList<Foodstuff> allFoodstuffs;
		IList<Recipe> allRecipes;

		internal FoodController(IFoodRepository foodRepo) {
			this.foodRepo = foodRepo;
		}

		internal void ShowView() {
			allFoodstuffs = foodRepo.GetAllFoodstuffs();
			allRecipes = foodRepo.GetAllRecipes();
            foodView = new FoodView(this, allFoodstuffs, allRecipes);
			foodView.ShowDialog();
		}

		#region Foodstuffs
		internal void OpenAddFoodstuffDialog() {
			var addFoodstuffDialog = new AddOrEditFoodstuffDialog(this);
			ShowAddOrEditFoodstuffDialog(addFoodstuffDialog);
        }

		internal void OpenEditFoodstuffDialog(Foodstuff currentFoodstuff) {
			var editFoodstuffDialog = 
				new AddOrEditFoodstuffDialog(this, currentFoodstuff);
			ShowAddOrEditFoodstuffDialog(editFoodstuffDialog);
        }

		private void ShowAddOrEditFoodstuffDialog(AddOrEditFoodstuffDialog dialog) {
			var result = dialog.ShowDialog();
			if (result == DialogResult.OK) {
				foodRepo.SaveOrUpdate(dialog.Foodstuff);
				RefreshFoodstuffs();
            }
		}

		internal void DeleteFoodstuff(Foodstuff foodstuff) {
			try {
				foodRepo.Delete(foodstuff);
			}
			catch {
				MessageBox.Show("Ne može se izbrisati namirnica, jer je unesena kao konzumirana hrana.");
			}
			RefreshFoodstuffs();
        }

		private void RefreshFoodstuffs() {
			allFoodstuffs = foodRepo.GetAllFoodstuffs();
			foodView.UpdateView(allFoodstuffs);
		}
		#endregion

		#region Recipes
		internal void OpenAddRecipeDialog() {
			var recipeController = new RecipeController(this, foodRepo);
			recipeController.ShowView();
			RefreshRecipes();
		}

		internal void OpenEditRecipeDialog(Recipe currentRecipe) {
			var recipeController = new RecipeController(this, foodRepo, currentRecipe);
			recipeController.ShowView();
			RefreshRecipes();
		}

		internal void DeleteRecipe(Recipe recipe) {
			try {
				foodRepo.Delete(recipe);
			}
			catch {
				MessageBox.Show("Ne može se izbrisati recept, jer je unesena kao konzumirana hrana.");
			}			
			RefreshRecipes();
		}

		private void RefreshRecipes() {
			allRecipes = foodRepo.GetAllRecipes();
			foodView.UpdateView(allRecipes);
		}
		#endregion

		internal bool NameNotUnique(int foodId, string name) {
			return allFoodstuffs.Any(f => f.Name.Equals(name) && f.Id != foodId)
					|| allRecipes.Any(r => r.Name.Equals(name) && r.Id != foodId);
		}
	}
}
