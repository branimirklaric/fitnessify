﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitnessify.Model;
using System.Windows.Forms;

namespace Fitnessify {
	public class RecipeController {
		FoodController foodController;
		IFoodRepository foodRepo;
		Recipe recipe;
		AddOrEditRecipeDialog editRecipeDialog;

		internal RecipeController(FoodController foodController, 
				IFoodRepository foodRepo) {
			this.foodController = foodController;
            this.foodRepo = foodRepo;
			recipe = new Recipe("Ime recepta");
		}

		internal RecipeController(FoodController foodController, 
				IFoodRepository foodRepo, Recipe recipe) {
			this.foodController = foodController;
            this.foodRepo = foodRepo;
			this.recipe = recipe;
		}

		internal void ShowView() {
			editRecipeDialog = new AddOrEditRecipeDialog(this, recipe);
			var result = editRecipeDialog.ShowDialog();
			if (result == DialogResult.OK) {
				foodRepo.SaveOrUpdate(recipe);
			}
		}

		internal void OpenAddIngredientDialog() {
			var foodStuffs = foodRepo.GetAllFoodstuffs();
			var addIngredientDialog = new AddIngredientDialog(this, foodStuffs);
			var result = addIngredientDialog.ShowDialog();
			if (result == DialogResult.OK) {
				recipe.Add(addIngredientDialog.Ingredient);
				editRecipeDialog.RefreshRecipe(recipe);
			}
		}

		internal void DeleteIngredient(Ingredient ingredient) {
			recipe.Remove(ingredient);
			editRecipeDialog.RefreshRecipe(recipe);
		}

		internal bool IngredientAlreadyAdded(string ingredientName) {
			return recipe.Ingredients.Any(i => i.Foodstuff.Name.Equals(ingredientName));
		}

		internal bool NameNotUnique(string name) {
			return foodController.NameNotUnique(recipe.Id, name);
		}
    }
}
