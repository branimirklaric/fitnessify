﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	public class DayController {
		IDayRepository dayRepo;
		IFoodRepository foodRepo;
		IPhysicalActivityRepository activityRepo;
		IPersonalDataRepository personalDataRepo;
        DayView dayView;
		Model.Day currentDay;
		PersonalData personalData;

		#region Day encapsulation
		public virtual int Calories {
			get {
				return currentDay.Calories;
			}
		}

		public virtual int Proteins {
			get {
				return currentDay.Proteins;
			}
		}

		public virtual int Carbs {
			get {
				return currentDay.Carbs;
			}
		}

		public virtual int Fats {
			get {
				return currentDay.Fats;
			}
		}

		public virtual int Water {
			get {
				return currentDay.Water;
			}
		}

		public virtual decimal ProteinsPercentage {
			get {
				return currentDay.ProteinsPercentage * 100;
			}
		}

		public virtual decimal CarbsPercentage {
			get {
				return currentDay.CarbsPercentage * 100;
			}
		}

		public virtual decimal FatsPercentage {
			get {
				return currentDay.FatsPercentage * 100;
			}
		}
		#endregion

		public DayController(IDayRepository dayRepo, IFoodRepository foodRepo, 
				IPhysicalActivityRepository activityRepo, 
				IPersonalDataRepository personalDataRepo) {
			this.dayRepo = dayRepo;
			this.foodRepo = foodRepo;
			this.activityRepo = activityRepo;
			this.personalDataRepo = personalDataRepo;
        }

		internal DayView GetView() {
			currentDay = dayRepo.GetByDate(DateTime.Today);
			personalData = personalDataRepo.Get();
            dayView = new DayView(this, currentDay, personalData);
            return dayView;
		}

		internal Model.Day GetDay(DateTime datetime) {
			currentDay = dayRepo.GetByDate(datetime);
			return currentDay;
		}

		#region Food
		internal void OpenAddConsumedFoodDialog() {
			var allFood = foodRepo.GetAllFoods();
            var addConsumedFoodDialog = new AddConsumedFoodDialog(allFood);
			var result = addConsumedFoodDialog.ShowDialog();
			if (result == DialogResult.OK) {
				currentDay.Add(addConsumedFoodDialog.ConsumedFood);
				dayRepo.SaveOrUpdate(currentDay);
				dayView.UpdateDay(currentDay);
			}
		}

		internal void DeleteConsumedFood(ConsumedFood consumedFood) {
			currentDay.Remove(consumedFood);
			dayRepo.SaveOrUpdate(currentDay);
			dayView.UpdateDay(currentDay);
		}

		internal void OpenFoodManagementView() {
			var foodController = new FoodController(foodRepo);
			foodController.ShowView();
			dayView.UpdateDay(currentDay);
		}
		#endregion

		#region Activities
		internal void OpenAddDoneActivityDialog() {
			var allActivities = activityRepo.GetAll();
			var addDoneActivityDialog = new AddDoneActivityDialog(allActivities);
			var result = addDoneActivityDialog.ShowDialog();
			if (result == DialogResult.OK) {
				currentDay.Add(addDoneActivityDialog.DoneActivity);
				dayRepo.SaveOrUpdate(currentDay);
				dayView.UpdateDay(currentDay);
			}
		}

		internal void DeleteDoneActivity(IDoneActivity doneActivity) {
			currentDay.Remove(doneActivity);
			dayRepo.SaveOrUpdate(currentDay);
			dayView.UpdateDay(currentDay);
		}

		internal void OpenActivitiesManagementView() {
			MessageBox.Show("Nije implementirano zbog manjka vremena i jer bi svakako bilo copy-paste upravljanja hranom.");
		}
		#endregion

		internal void OpenPersonalDataDialog() {
			var personalDataDialog = new PersonalDataDialog(personalData);
			var result = personalDataDialog.ShowDialog();
			if (result == DialogResult.OK) {
				personalData = personalDataDialog.PersonalData;
				personalDataRepo.Save(personalData);
				dayView.UpdatePersonalData(personalData);
            }
		}

		internal int CaloricBalance() {
			return Calories - currentDay.EnergyExpenditure();
		}
    }
}
