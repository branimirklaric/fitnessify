﻿using Fitnessify.Mapping;
using Fitnessify.Model;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitnessify {
	static class Program {
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			var sessionFactory = Fluently.Configure()
				.Database(SQLiteConfiguration.Standard.UsingFile("baza"))
				.Mappings(m => m.FluentMappings.AddFromAssemblyOf<PersonalDataMap>())
				//.ExposeConfiguration(c => new SchemaExport(c).Create(true, true))
				.BuildSessionFactory();
			//InitDb(sessionFactory);

			var dayRepository = new DayRepository(sessionFactory);
			var foodRepository = new FoodRepository(sessionFactory);
			var activityRepo = new PhysicalActivityRepository(sessionFactory);
			var personalDataRepo = new PersonalDataRepository(sessionFactory);

			var dayController = new DayController(
				dayRepository, foodRepository, activityRepo, personalDataRepo);

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Application.Run(dayController.GetView());
		}

		static void InitDb(ISessionFactory sessionFactory) {
			var jabuka = new Foodstuff("Jabuka", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 20 },
						{ Nutrient.Fats, 2 },
						{ Nutrient.Proteins, 5 }
					});
			var mlijeko = new Foodstuff("Mlijeko", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 5 },
						{ Nutrient.Fats, 3 },
						{ Nutrient.Proteins, 3 }
					});
			var janjetina = new Foodstuff("Janjetina", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 10 },
						{ Nutrient.Fats, 30 },
						{ Nutrient.Proteins, 40 }
					});
			var ingredient = new Ingredient(jabuka, 200);
			var vocnaSalata = new Recipe("Voćna salata", 
				new List<Ingredient> { ingredient });
			var trcanje = new Exercise("Trčanje", 100);
			var sklekovi = new Exercise("Sklekovi", 500);
			var trcanjeRoutineEx = new RoutineExercise(trcanje, 30);
			var sklekoviRoutineEx = new RoutineExercise(sklekovi, 5);
			var poligon = new Routine("Poligon", 
				new List<RoutineExercise> { trcanjeRoutineEx, sklekoviRoutineEx });
			var data = new PersonalData(186, 90, 22, Gender.Male, 85, 90);

			using (var session = sessionFactory.OpenSession()) {
				using (var transaction = session.BeginTransaction()) {
					session.Save(jabuka);
					session.Save(mlijeko);
					session.Save(janjetina);
					session.Save(vocnaSalata);
					session.Save(sklekovi);
					session.Save(trcanje);
					session.Save(poligon);
					session.Save(data);
					transaction.Commit();
				}
			}
		}
	}
}
