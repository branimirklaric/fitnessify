﻿using Fitnessify.Model;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify {
	public class FoodRepository : IFoodRepository {
		ISessionFactory sessionFactory;

		public FoodRepository(ISessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

		public IList<Food> GetAllFoods() {
			using (var session = sessionFactory.OpenSession()) {
				return session.QueryOver<Food>().List();
			}
		}

		public IList<Foodstuff> GetAllFoodstuffs() {
			using (var session = sessionFactory.OpenSession()) {
				return session.QueryOver<Foodstuff>().List();
			}
		}

		public IList<Recipe> GetAllRecipes() {
			using (var session = sessionFactory.OpenSession()) {
				return session.QueryOver<Recipe>().List();
			}
		}

		public void SaveOrUpdate(Foodstuff foodstuff) {
			using (var session = sessionFactory.OpenSession()) {
				using (var transaction = session.BeginTransaction()) {
					session.SaveOrUpdate(foodstuff);
					transaction.Commit();
				}
			}
		}

		public void SaveOrUpdate(Recipe recipe) {
			using (var session = sessionFactory.OpenSession()) {
				using (var transaction = session.BeginTransaction()) {
					session.SaveOrUpdate(recipe);
					transaction.Commit();
				}
			}
		}

		public void Delete(Foodstuff foodstuff) {
			using (var session = sessionFactory.OpenSession()) {
				using (var transaction = session.BeginTransaction()) {
					session.Delete(foodstuff);
					transaction.Commit();
				}
			}
		}

		public void Delete(Recipe recipe) {
			using (var session = sessionFactory.OpenSession()) {
				using (var transaction = session.BeginTransaction()) {
					session.Delete(recipe);
					transaction.Commit();
				}
			}
		}
	}
}
