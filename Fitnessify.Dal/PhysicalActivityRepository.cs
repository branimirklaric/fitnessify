﻿using Fitnessify.Model;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify {
	public class PhysicalActivityRepository : IPhysicalActivityRepository {
		ISessionFactory sessionFactory;

		public PhysicalActivityRepository(ISessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

		public IList<PhysicalActivity> GetAll() {
			using (var session = sessionFactory.OpenSession()) {
				return session.QueryOver<PhysicalActivity>().List();
			}
		}
	}
}
