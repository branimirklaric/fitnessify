﻿using Fitnessify.Model;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify {
	public class DayRepository : IDayRepository {
		ISessionFactory sessionFactory;
		ISession session;

		public DayRepository(ISessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
			session = sessionFactory.OpenSession();
		}

		public Day GetByDate(DateTime date) {
			return session.QueryOver<Day>()
					.Where(d => d.Date.Day == date.Day && d.Date.Month == date.Month)
					.SingleOrDefault() ?? new Day(date);
		}

		public void SaveOrUpdate(Day day) {
			using (var transaction = session.BeginTransaction()) {
				session.SaveOrUpdate(day);
				transaction.Commit();
			}
		}
	}
}
