﻿using Fitnessify.Model;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify {
	public class PersonalDataRepository : IPersonalDataRepository {
		ISessionFactory sessionFactory;

		public PersonalDataRepository(ISessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

		public PersonalData Get() {
			using (var session = sessionFactory.OpenSession()) {
				return session.QueryOver<PersonalData>().SingleOrDefault();
			}
		}

		public void Save(PersonalData data) {
			using (var session = sessionFactory.OpenSession()) {
				using (var transaction = session.BeginTransaction()) {
					session.Delete("from PersonalData e");
					session.Save(data);
					transaction.Commit();
				}
			}
		}
	}
}
