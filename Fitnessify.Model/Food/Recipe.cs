﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public class Recipe : Food {
        IList<Ingredient> ingredients;

		public virtual IEnumerable<Ingredient> Ingredients {
			get { return ingredients; }
		}

        public override int Calories {
            get { return ToPerHundredGrams(i => i.Calories); }
        }

        public override int Proteins {
            get { return ToPerHundredGrams(i => i.Proteins); }
        }

        public override int Carbs {
            get { return ToPerHundredGrams(i => i.Carbs); }
        }

        public override int Fats {
            get { return ToPerHundredGrams(i => i.Fats); }
        }

        public override int Water {
            get { return ToPerHundredGrams(i => i.Water); }
        }

		public override string ListInfo {
			get { return "R: " + Name; }
		}

		protected Recipe() { }

		public Recipe(string name) : base(name) {
			ingredients = new List<Ingredient>();
		}

        public Recipe(string name, IList<Ingredient> ingredients) : base(name) {
			NotNullEnsurer.Ensure(ingredients);
			ValidateIngredients(ingredients);
            this.ingredients = ingredients;
        }

        int ToPerHundredGrams(Func<Ingredient, int> selector) {
            var totalValue = ingredients.Sum(selector);
            var totalMass = ingredients.Sum(i => i.Mass);
			if (totalMass == 0) {
				return 0;
			}
            var valuePerHundredGrams = totalValue * 100 / totalMass;
            return valuePerHundredGrams;
        }

		public virtual void Add(Ingredient item) {
			ValidateIngredient(item, ingredients);
			ingredients.Add(item);
		}

		public virtual bool Remove(Ingredient item) {
			return ingredients.Remove(item);
		}

        void ValidateIngredients(IList<Ingredient> ingredients) {
            foreach (var ingredient in ingredients) {
                ValidateIngredient(ingredient, ingredients);
            }
        }

        void ValidateIngredient(Ingredient ingredient, IList<Ingredient> ingredients) {
			NotNullEnsurer.Ensure(ingredient);
            if (ingredients.Where(i => i.Id != ingredient.Id).Any(
					i => i.Foodstuff.Name == ingredient.Foodstuff.Name)) {
                throw new ArgumentException("Ingredient already present in recipe.");
            }
        }

		public override bool Equals(object obj) {
			var objAsRecipe = obj as Recipe;
			return objAsRecipe != null && objAsRecipe.Id == Id;
		}
	}
}
