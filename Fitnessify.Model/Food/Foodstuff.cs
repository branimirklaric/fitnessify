﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public class Foodstuff : Food {
        int proteins;
        int carbs;
        int fats;
        int water;

        public override int Calories {
            get { return 4 * Proteins + 4 * Carbs + 9 * Fats; }
        }

        public override int Proteins {
            get { return proteins; }
        }

        public override int Carbs {
            get { return carbs; }
        }

        public override int Fats {
            get { return fats; }
        }

        public override int Water {
            get { return water; }
        }

		public override string ListInfo {
			get { return "F: " + Name; }
		}

		protected Foodstuff() { }

		public Foodstuff(string name)
				: base(name) {
			proteins = 10;
			carbs = 10;
			fats = 10;
			water = 70;
		}

		public Foodstuff(string name, IDictionary<Nutrient, int> nutritionalValues)
                : base(name) {
            SetNutritionalValues(nutritionalValues);
        }

        public virtual void SetNutritionalValues(
                IDictionary<Nutrient, int> nutritionalValues) {
            ValidateNutriotionalValues(nutritionalValues);
            if (nutritionalValues.Keys.Count == 3) {
                AddMissingNutrient(nutritionalValues);
            }
            proteins = nutritionalValues[Nutrient.Proteins];
            carbs = nutritionalValues[Nutrient.Carbs];
            fats = nutritionalValues[Nutrient.Fats];
            water = nutritionalValues[Nutrient.Water];
        }

        void ValidateNutriotionalValues(IDictionary<Nutrient, int> nutritionalValues) {
            if (nutritionalValues == null) {
                throw new ArgumentNullException("nutritionalValues");
            }
            if (nutritionalValues.Keys.Count < 3 || nutritionalValues.Keys.Count > 4) {
                throw new ArgumentException("Invalid number of nutritional values.");
            }
            if (nutritionalValues.Values.Sum() > 100) {
                throw new ArgumentException("Invalid mass of nutritional values.");
            }
            if (nutritionalValues.Keys.Count !=
                    nutritionalValues.Keys.Distinct().Count()) {
                throw new ArgumentException("Invalid number of distinct nutrients.");
            }
        }

        void AddMissingNutrient(IDictionary<Nutrient, int> nutritionalValues) {
            var allNutrients = new Nutrient[] {
                    Nutrient.Proteins,
                    Nutrient.Carbs,
                    Nutrient.Fats,
                    Nutrient.Water
                };
            var missingNutrient =
                allNutrients.Except(nutritionalValues.Keys).Single();
            var missingNutrientMass = 100 - nutritionalValues.Values.Sum();
            nutritionalValues.Add(missingNutrient, missingNutrientMass);
        }

		public override bool Equals(object obj) {
			var objAsFoodstuff = obj as Foodstuff;
			return objAsFoodstuff != null && objAsFoodstuff.Id == Id;
		}
	}
}
