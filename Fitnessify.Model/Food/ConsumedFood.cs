﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public class ConsumedFood : INutritionInfo {
        Food food;
        int mass;

		public virtual int Id { get; protected set; }

		public virtual Food Food {
            get {
				return food;
			}

			protected set {
				NotNullEnsurer.Ensure(value);
				food = value;
			}
        }

        public virtual int Mass {
            get {
				return mass;
			}

			protected set {
				GreaterThanZeroEnsurer.Ensure(value);
				mass = value;
			}
        }

		public virtual int Calories {
			get {
				return Food.Calories * Mass / 100;
			}
		}

		public virtual int Proteins {
			get {
				return Food.Proteins * Mass / 100;
			}
		}

		public virtual int Carbs {
			get {
				return Food.Carbs * Mass / 100;
			}
		}

		public virtual int Fats {
			get {
				return Food.Fats * Mass / 100;
			}
		}

		public virtual int Water {
			get {
				return Food.Water * Mass / 100;
			}
		}

		public virtual string ListInfo {
			get { return Food.ListInfo + " - " + Mass + "g"; }
		}

		protected ConsumedFood() { }

		public ConsumedFood(Food food, int mass) {
            Food = food;
            Mass = mass;
        }
    }
}