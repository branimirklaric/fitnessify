﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public interface INutritionInfo {
		int Calories { get; }

		int Proteins { get; }

		int Carbs { get; }

		int Fats { get; }

		int Water { get; }
	}
}
