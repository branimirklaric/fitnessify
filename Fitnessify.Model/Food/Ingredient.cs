﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public class Ingredient : INutritionInfo {
        Foodstuff foodstuff;
        int mass;

		public virtual int Id { get; protected set; }

		public virtual Foodstuff Foodstuff {
            get {
				return foodstuff;
			}

			protected set {
				NotNullEnsurer.Ensure(value);
				foodstuff = value;
			}
        }

        public virtual int Mass {
            get {
				return mass;
			}

			protected set {
				GreaterThanZeroEnsurer.Ensure(value);
				mass = value;
			}
        }

		public virtual int Calories {
			get {
				return Foodstuff.Calories * Mass / 100;
			}
		}

		public virtual int Proteins {
			get {
				return Foodstuff.Proteins * Mass / 100;
			}
		}

		public virtual int Carbs {
			get {
				return Foodstuff.Carbs * Mass / 100;
			}
		}

		public virtual int Fats {
			get {
				return Foodstuff.Fats * Mass / 100;
			}
		}

		public virtual int Water {
			get {
				return Foodstuff.Water * Mass / 100;
			}
		}

		public virtual string ListInfo {
			get {
				return Foodstuff.Name + " - " + Mass + " g";
			}
		}

		protected Ingredient() { }

		public Ingredient(Foodstuff foodstuff, int mass) {
            Foodstuff = foodstuff;
            Mass = mass;
        }
    }
}
