﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public abstract class Food : INutritionInfo {
        string name;

		public virtual int Id { get; protected set; }

		public virtual string Name {
            get {
                return name;
            }
            set {
                ValidateName(value);
                name = value;
            }
        }

		public abstract int Calories { get; }

		public abstract int Proteins { get; }

		public abstract int Carbs { get; }

		public abstract int Fats { get; }

		public abstract int Water { get; }

		public abstract string ListInfo { get; }

		protected Food() { }

		protected Food(string name) {
            Name = name;
        }

        void ValidateName(string name) {
            if (string.IsNullOrWhiteSpace(name)) {
                throw new ArgumentNullException("name");
            }
        }
    }
}
