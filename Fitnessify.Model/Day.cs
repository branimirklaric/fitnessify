﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public class Day : INutritionInfo, IEnergyExpender {
		DateTime date;
		IList<ConsumedFood> consumedFoods;
		IList<IDoneActivity> doneActivites;

		public virtual int Id { get; protected set; }

		public virtual DateTime Date {
			get {
				return date;
			}
		}

		public virtual IEnumerable<ConsumedFood> ConsumedFoods {
			get {
				return consumedFoods;
			}
		}

		public virtual IEnumerable<IDoneActivity> DoneActivites {
			get {
				return doneActivites;
			}
		}

		//YAGNI, gdje je 'It' keširanje
		public virtual int Calories {
			get {
				return consumedFoods.Sum(cf => cf.Calories);
			}
		}

		public virtual int Proteins {
			get {
				return consumedFoods.Sum(cf => cf.Proteins);
			}
		}

		public virtual int Carbs {
			get {
				return consumedFoods.Sum(cf => cf.Carbs);
			}
		}

		public virtual int Fats {
			get {
				return consumedFoods.Sum(cf => cf.Fats);
			}
		}

		public virtual int Water {
			get {
				return consumedFoods.Sum(cf => cf.Water);
			}
		}

		public virtual decimal ProteinsPercentage {
			get {
				var sum = Proteins + Carbs + Fats;
				if (sum == 0) {
					return 0.00m;
				}
				return (decimal)Proteins / (Proteins + Carbs + Fats);
			}
		}

		public virtual decimal CarbsPercentage {
			get {
				var sum = Proteins + Carbs + Fats;
				if (sum == 0) {
					return 0.00m;
				}
				return (decimal)Carbs / (Proteins + Carbs + Fats);
			}
		}

		public virtual decimal FatsPercentage {
			get {
				var sum = Proteins + Carbs + Fats;
				if (sum == 0) {
					return 0.00m;
				}
				return (decimal)Fats / (Proteins + Carbs + Fats);
			}
		}

		protected Day() { }

		public Day(DateTime date) {
			NotNullEnsurer.Ensure(date);
			this.date = date.Date;
            consumedFoods = new List<ConsumedFood>();
			doneActivites = new List<IDoneActivity>();
		}

		public Day(DateTime date, IList<ConsumedFood> consumedFoods,
				IList<IDoneActivity> doneActivites) {
			NotNullEnsurer.Ensure(date);
			NotNullEnsurer.Ensure(consumedFoods);
			NotNullEnsurer.Ensure(doneActivites);
			this.date = date;
			this.consumedFoods = consumedFoods;
			this.doneActivites = doneActivites;
		}

		public virtual int EnergyExpenditure() {
			return doneActivites.Sum(da => da.EnergyExpenditure());
		}

		public virtual void Add(ConsumedFood item) {
			NotNullEnsurer.Ensure(item);
			consumedFoods.Add(item);
		}

		public virtual bool Remove(ConsumedFood item) {
			return consumedFoods.Remove(item);
		}

		public virtual void Add(IDoneActivity item) {
			NotNullEnsurer.Ensure(item);
			doneActivites.Add(item);
		}

		public virtual bool Remove(IDoneActivity item) {
			return doneActivites.Remove(item);
		}
	}
}
