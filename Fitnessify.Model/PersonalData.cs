﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;



namespace Fitnessify.Model {
	public class PersonalData {
		int height;
		decimal weight;
		int age;
		private Gender gender;
		int? goalWeight;
		int? goalWeightDeadline;

		public virtual int Id { get; protected set; }

		public virtual int Height {
			get {
				return height;
			}

			set {
				GreaterThanZeroEnsurer.Ensure(value);
				height = value;
			}
		}

		public virtual decimal Weight {
			get {
				return weight;
			}

			set {
				GreaterThanZeroEnsurer.Ensure(value);
				weight = value;
			}
		}

		public virtual int Age {
			get {
				return age;
			}

			set {
				GreaterThanZeroEnsurer.Ensure(value);
				age = value;
			}
		}
		
		public virtual Gender Gender {
			get {
				return gender;
			}

			set {
				gender = value;
			}
		}

		public virtual int? GoalWeight {
			get {
				return goalWeight;
			}

			set {
				if (value != null) {
					GreaterThanZeroEnsurer.Ensure((int)value);
				}
				goalWeight = value;
			}
		}

		public virtual int? GoalWeightDeadline {
			get {
				return goalWeightDeadline;
			}

			set {
				if (value != null) {
					GreaterThanZeroEnsurer.Ensure((int)value);
				}
				goalWeightDeadline = value;
			}
		}

		protected PersonalData() { }

		public PersonalData(int height, decimal weight, int age, Gender gender) {
			Height = height;
			Weight = weight;
			Age = age;
			Gender = gender;
			GoalWeight = null;
			GoalWeightDeadline = null;
		}

		public PersonalData(int height, decimal weight, int age, Gender gender,
				int? goalWeight, int? goalWeightDeadline) {
			Height = height;
			Weight = weight;
			Age = age;
			Gender = gender;
			GoalWeight = goalWeight;
			GoalWeightDeadline = goalWeightDeadline;
		}

		public virtual decimal Bmi() {
			return Weight / (Height / 100m * Height / 100m);
		}

		public virtual int BasalMetabolism() {
			return Gender == Gender.Male ?
				(int)(66 + (13.7m * Weight) + (5m * Height) - (6.8m * Age)) :
				(int)(655 + (9.6m * Weight) + (1.8m * Height) - (4.7m * Age));
		}

		public virtual int? DailyCaloricDeficit() {
			return (int?)((Weight - GoalWeight) * 7000 / GoalWeightDeadline);
		}
	}
}
