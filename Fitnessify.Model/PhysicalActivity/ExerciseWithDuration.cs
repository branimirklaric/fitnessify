﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public abstract class ExerciseWithDuration : IEnergyExpender {
        Exercise exercise;
        int duration;

		public virtual int Id { get; protected set; }

		public virtual Exercise Exercise {
            get {
                return exercise;
            }

            protected set {
				NotNullEnsurer.Ensure(value);
                exercise = value;
            }
        }

        public virtual int Duration {
            get {
                return duration;
            }

			protected set {
				GreaterThanZeroEnsurer.Ensure(value);
				duration = value;
            }
        }

		protected ExerciseWithDuration() { }

        protected ExerciseWithDuration(Exercise exercise, int duration) {
            Exercise = exercise;
            Duration = duration;
        }

		public virtual int EnergyExpenditure() {
			return Exercise.EnergyExpenditure() * Duration / 60;
		}
	}
}
