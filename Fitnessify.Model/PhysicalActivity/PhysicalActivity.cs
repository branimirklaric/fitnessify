﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public abstract class PhysicalActivity : IEnergyExpender {
        string name;

		public virtual int Id { get; protected set; }

		public virtual string Name {
            get {
                return name;
            }
            set {
                ValidateName(value);
                name = value;
            }
        }

		public abstract string ListInfo { get; }

		protected PhysicalActivity() { }

		protected PhysicalActivity(string name) {
            Name = name;
        }

        void ValidateName(string name) {
            if (string.IsNullOrWhiteSpace(name)) {
                throw new ArgumentNullException("name");
            }
        }

        public abstract int EnergyExpenditure();
    }
}
