﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public class RoutineExercise : ExerciseWithDuration {
		protected RoutineExercise() { }

        public RoutineExercise(Exercise exercise, int duration) 
                : base(exercise, duration) {
        }
	}
}
