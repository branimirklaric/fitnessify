﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public class Routine : PhysicalActivity {
		IList<RoutineExercise> exercises;

		public virtual IEnumerable<RoutineExercise> Exercises {
			get { return exercises; }
		}

		public override string ListInfo {
			get {
				return "R: " + Name;
			}
		}

		protected Routine() { }

		public Routine(string name, IList<RoutineExercise> exercises) : base(name) {
			NotNullEnsurer.Ensure(exercises);
			ValidateExercises(exercises);
			this.exercises = exercises;
		}

		public virtual void Add(RoutineExercise exercise) {
			NotNullEnsurer.Ensure(exercise);
			exercises.Add(exercise);
		}

		public virtual void Remove(RoutineExercise exercise) {
			exercises.Remove(exercise);
		}

		public override int EnergyExpenditure() {
			return exercises.Sum(e => e.EnergyExpenditure());
		}

		void ValidateExercises(IList<RoutineExercise> exercises) {
			foreach (var exercise in exercises) {
				NotNullEnsurer.Ensure(exercise);
			}
		}
	}
}
