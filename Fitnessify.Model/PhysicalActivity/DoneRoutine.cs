﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public class DoneRoutine : IDoneActivity {
		Routine routine;
		int repetitions;

		public virtual int Id { get; protected set; }

		public virtual Routine Routine {
			get {
				return routine;
			}

			protected set {
				NotNullEnsurer.Ensure(value);
				routine = value;
			}
		}

		public virtual int Repetitions {
			get {
				return repetitions;
			}

			protected set {
				GreaterThanZeroEnsurer.Ensure(value);
				repetitions = value;
			}
		}

		public virtual string ListInfo {
			get {
				return "R: " + Routine.Name + " - " + Repetitions + " ponavljanja";
			}
		}

		protected DoneRoutine() { }

		public DoneRoutine(Routine routine, int repetitions) {
			Routine = routine;
			Repetitions = repetitions;
		}

		public virtual int EnergyExpenditure() {
			return routine.EnergyExpenditure() * repetitions;
		}
	}
}
