﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public interface IDoneActivity : IEnergyExpender {
		string ListInfo { get; }
    }
}
