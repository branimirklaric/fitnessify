﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public class DoneExercise : ExerciseWithDuration, IDoneActivity {
		public virtual string ListInfo {
			get {
				return "E: " + Exercise.Name + " - " + Duration + " minuta";
			}
		}

		protected DoneExercise() { }

		public DoneExercise(Exercise exercise, int duration) 
				: base(exercise, duration) { }
	}
}
