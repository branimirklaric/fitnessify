﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
    public class Exercise : PhysicalActivity {
        int calsPerHour;

        public virtual int CalsPerHour {
            get {
                return calsPerHour;
            }

            set {
                ValidateCalsPerHour(value);
                calsPerHour = value;
            }
        }

		public override string ListInfo {
			get {
				return "E: " + Name;
			}
		}

		protected Exercise() { }

        public Exercise(string name, int calsPerHour) : base(name) {
            CalsPerHour = calsPerHour;
        }

        void ValidateCalsPerHour(int calsPerHour) {
            if (calsPerHour < 1) {
                throw new ArgumentException("An exercise must expend calories.");
            }
        }

        public override int EnergyExpenditure() {
            return CalsPerHour;
        }
    }
}
