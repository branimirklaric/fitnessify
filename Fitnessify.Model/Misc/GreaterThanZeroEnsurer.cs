﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify {
	class GreaterThanZeroEnsurer {
		public static void Ensure(int arg) {
			if (arg < 1) {
				throw new ArgumentException("Integer must be greater than zero.");
			}
		}

		public static void Ensure(decimal arg) {
			if (arg < 1m) {
				throw new ArgumentException("Integer must be greater than zero.");
			}
		}
	}
}
