﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify {
	public static class NotNullEnsurer {
		public static void Ensure(object arg) {
			if (arg == null) {
				throw new ArgumentNullException();
			}
		}
	}
}
