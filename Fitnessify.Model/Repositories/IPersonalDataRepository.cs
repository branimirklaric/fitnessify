﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public interface IPersonalDataRepository {
		void Save(PersonalData data);

		PersonalData Get();
	}
}
