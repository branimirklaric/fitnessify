﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public interface IDayRepository {
		Day GetByDate(DateTime date);

		void SaveOrUpdate(Day day);
    }
}
