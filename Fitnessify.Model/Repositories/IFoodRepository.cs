﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Model {
	public interface IFoodRepository {
		IList<Food> GetAllFoods();

		IList<Foodstuff> GetAllFoodstuffs();

		IList<Recipe> GetAllRecipes();

		void SaveOrUpdate(Foodstuff foodstuff);

		void SaveOrUpdate(Recipe recipe);

		void Delete(Foodstuff foodstuff);

		void Delete(Recipe recipe);
	}
}
