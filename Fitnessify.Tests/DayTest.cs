﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Fitnessify.Tests {
	public class DayTest {
		[Fact]
		public void DayGeneralTest() {
			var jabuka = new Foodstuff("Jabuka", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 20 },
						{ Nutrient.Fats, 2 },
						{ Nutrient.Proteins, 5 }
					});
			var mlijeko = new Foodstuff("Mlijeko", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 5 },
						{ Nutrient.Fats, 3 },
						{ Nutrient.Proteins, 3 }
					});
			var jabukaIngredient = new Ingredient(jabuka, 200);
			var mlijekoIngredient = new Ingredient(mlijeko, 100);
			var vocnaSalata = new Recipe("Voćna salata",
				new List<Ingredient> { jabukaIngredient, mlijekoIngredient });

			var trcanje = new Exercise("Trčanje", 100);
			var sklekovi = new Exercise("Sklekovi", 500);
			var trcanjeRoutineEx = new RoutineExercise(trcanje, 30);
			var sklekoviRoutineEx = new RoutineExercise(sklekovi, 5);
			var poligon = new Routine("Poligon",
				new List<RoutineExercise> { trcanjeRoutineEx, sklekoviRoutineEx });

			var consumedJabuka = new ConsumedFood(jabuka, 50);
			var consumedVocnaSalata = new ConsumedFood(vocnaSalata, 200);
			var doneTrcanje = new DoneExercise(trcanje, 20);
			var donePoligon = new DoneRoutine(poligon, 3);

			var consumedFoods = new List<ConsumedFood>() {
				consumedJabuka, consumedVocnaSalata
			};
			var doneActivities = new List<IDoneActivity>() {
				doneTrcanje, donePoligon
			};

			var date = new DateTime(2016, 1, 1);

			var day = new Day(date, consumedFoods, doneActivities);

			Assert.Equal(255, day.Calories);
			Assert.Equal(10, day.Proteins);
			Assert.Equal(40, day.Carbs);
			Assert.Equal(5, day.Fats);
			Assert.Equal(192, day.Water);
			Assert.Equal(0.18m, decimal.Round(day.ProteinsPercentage, 2));
			Assert.Equal(0.73m, decimal.Round(day.CarbsPercentage, 2));
			Assert.Equal(0.09m, decimal.Round(day.FatsPercentage, 2));
			Assert.Equal(306, day.EnergyExpenditure());
			Assert.Equal(2, day.ConsumedFoods.Count());
			Assert.Equal(2, day.DoneActivites.Count());
		}
	}
}
