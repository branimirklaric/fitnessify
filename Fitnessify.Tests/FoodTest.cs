﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Fitnessify.Tests {
	public class FoodTest {
		[Fact]
		public void FoodstuffTest() {
			var jabuka = new Foodstuff("Jabuka", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 20 },
						{ Nutrient.Fats, 2 },
						{ Nutrient.Proteins, 5 }
					});

			Assert.Equal(118, jabuka.Calories);
			Assert.Equal(5, jabuka.Proteins);
			Assert.Equal(20, jabuka.Carbs);
			Assert.Equal(2, jabuka.Fats);
			Assert.Equal(73, jabuka.Water);
		}

		[Fact]
		public void RecipeTest() {
			var jabuka = new Foodstuff("Jabuka", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 20 },
						{ Nutrient.Fats, 2 },
						{ Nutrient.Proteins, 5 }
					});
			var mlijeko = new Foodstuff("Mlijeko", new Dictionary<Nutrient, int>() {
						{ Nutrient.Carbs, 5 },
						{ Nutrient.Fats, 3 },
						{ Nutrient.Proteins, 3 }
					});
			var jabukaIngredient = new Ingredient(jabuka, 200);
			var mlijekoIngredient = new Ingredient(mlijeko, 100);
			var vocnaSalata = new Recipe("Voćna salata", 
				new List<Ingredient> { jabukaIngredient, mlijekoIngredient });

			Assert.Equal(98, vocnaSalata.Calories);
			Assert.Equal(4, vocnaSalata.Proteins);
			Assert.Equal(15, vocnaSalata.Carbs);
			Assert.Equal(2, vocnaSalata.Fats);
			Assert.Equal(78, vocnaSalata.Water);
			Assert.Equal(2, vocnaSalata.Ingredients.Count());
		}
	}
}
