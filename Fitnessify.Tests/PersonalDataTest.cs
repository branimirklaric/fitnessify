﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Fitnessify.Tests {
	public class PersonalDataTest {
		[Fact]
		public void DataTest() {
			var data = new PersonalData(186, 90, 22, Gender.Male, 85, 90);

			Assert.Equal(26, (int)data.Bmi());
			Assert.Equal(2079, data.BasalMetabolism());
			Assert.Equal(388, data.DailyCaloricDeficit());
		}
	}
}
