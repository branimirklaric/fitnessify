﻿using Fitnessify.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Fitnessify.Tests {
	public class PhysicalActivityTest {
		[Fact]
		public void ExerciseTest() {
			var trcanje = new Exercise("Trčanje", 100);

			Assert.Equal(100, trcanje.EnergyExpenditure());
		}

		[Fact]
		public void RoutineTest() {
			var trcanje = new Exercise("Trčanje", 100);
			var sklekovi = new Exercise("Sklekovi", 500);
			var trcanjeRoutineEx = new RoutineExercise(trcanje, 30);
			var sklekoviRoutineEx = new RoutineExercise(sklekovi, 5);
			var poligon = new Routine("Poligon",
				new List<RoutineExercise> { trcanjeRoutineEx, sklekoviRoutineEx });

			Assert.Equal(91, poligon.EnergyExpenditure());
			Assert.Equal(2, poligon.Exercises.Count());
		}
	}
}
