﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class FoodstuffMap : SubclassMap<Foodstuff> {
		public FoodstuffMap() {
			KeyColumn("Id");
			Map(x => x.Proteins).Access.CamelCaseField();
			Map(x => x.Carbs).Access.CamelCaseField();
			Map(x => x.Fats).Access.CamelCaseField();
			Map(x => x.Water).Access.CamelCaseField();
        }
	}
}
