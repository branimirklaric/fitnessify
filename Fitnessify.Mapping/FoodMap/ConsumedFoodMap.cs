﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class ConsumedFoodMap : ClassMap<ConsumedFood> {
		public ConsumedFoodMap() {
			Id(x => x.Id);
			References(x => x.Food);
			Map(x => x.Mass);
		}
	}
}
