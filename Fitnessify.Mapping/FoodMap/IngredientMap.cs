﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class IngredientMap : ClassMap<Ingredient> {
		public IngredientMap() {
			Id(x => x.Id);
			References(x => x.Foodstuff)
				.Access.CamelCaseField()
				.Not.LazyLoad();
			Map(x => x.Mass).Not.Nullable();
		}
	}
}
