﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class FoodMap : ClassMap<Food> {
		public FoodMap() {
			Id(x => x.Id);
			Map(x => x.Name).Not.Nullable().Unique();
		}
	}
}
