﻿using Fitnessify.Model;
using FluentNHibernate;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class RecipeMap : SubclassMap<Recipe> {
		public RecipeMap() {
			KeyColumn("Id");
			HasMany(x => x.Ingredients)
				.Access.CamelCaseField()
				.Cascade.AllDeleteOrphan()
				.Not.LazyLoad();
		}
	}
}
