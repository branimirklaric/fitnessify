﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class DayMap : ClassMap<Day> {
        public DayMap() {
			Id(x => x.Id);
			Map(x => x.Date)
				.Access.CamelCaseField()
				.Not.Nullable()
				.Unique();
			HasMany(x => x.ConsumedFoods)
				.Access.CamelCaseField()
				.Cascade.AllDeleteOrphan();
			HasMany(x => x.DoneActivites)
				.Access.CamelCaseField()
				.Cascade.AllDeleteOrphan();
		}
	}
}
