﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	public class PersonalDataMap : ClassMap<PersonalData> {
		public PersonalDataMap() {
			Id(x => x.Id);
			Map(x => x.Height).Not.Nullable();
			Map(x => x.Weight).Not.Nullable();
			Map(x => x.Age).Not.Nullable();
			Map(x => x.Gender).Not.Nullable();
			Map(x => x.GoalWeight);
			Map(x => x.GoalWeightDeadline);
		}
	}
}
