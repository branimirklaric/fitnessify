﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class RoutineMap : SubclassMap<Routine> {
		public RoutineMap() {
			KeyColumn("Id");
			HasMany(x => x.Exercises)
				.Access.CamelCaseField()
				.Cascade.AllDeleteOrphan()
				.Not.LazyLoad();
		}
	}
}
