﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class ExerciseMap : SubclassMap<Exercise> {
		public ExerciseMap() {
			KeyColumn("Id");
			Map(x => x.CalsPerHour);
		}
	}
}
