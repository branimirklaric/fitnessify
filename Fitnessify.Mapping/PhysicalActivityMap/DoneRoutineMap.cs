﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class DoneRoutineMap : SubclassMap<DoneRoutine> {
		public DoneRoutineMap() {
			KeyColumn("id");
			References(x => x.Routine);
			Map(x => x.Repetitions);
		}
	}
}
