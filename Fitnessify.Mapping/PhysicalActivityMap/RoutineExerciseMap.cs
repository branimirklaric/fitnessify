﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class RoutineExerciseMap : ClassMap<RoutineExercise> {
		public RoutineExerciseMap() {
			Id(x => x.Id);
			References(x => x.Exercise)
				.Access.CamelCaseField()
				.Not.LazyLoad();
			Map(x => x.Duration).Not.Nullable();
		}
	}
}
