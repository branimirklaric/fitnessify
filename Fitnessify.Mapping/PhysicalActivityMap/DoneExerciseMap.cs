﻿using Fitnessify.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fitnessify.Mapping {
	class DoneExerciseMap : SubclassMap<DoneExercise> {
		public DoneExerciseMap() {
			KeyColumn("id");
			References(x => x.Exercise);
			Map(x => x.Duration).Not.Nullable();
		}
	}
}
